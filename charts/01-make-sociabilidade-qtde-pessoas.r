library(echarts4r)

df <- read.csv('processed data/01_LocalHorarioPessoas_20240315.csv')

df |> 
    group_by(local) |>
    e_charts(horario) |>
    e_area(quantidade_pessoas) |>
    e_title(
        "Pessoas por Hora e por Espaços", 
        "Quantidade de pessoas observadas nos espaços por período",
        textStyle = list(color="#000000")) |>
    e_color(c('#9DC6FB','#C97269','#E8442E')) |>
    e_legend(bottom = 0) |>
    e_tooltip(trigger = "axis") |>
    e_toolbox(right = 0) |>
    e_toolbox_feature(
        feature = "magicType", type = list("line", "bar"))
