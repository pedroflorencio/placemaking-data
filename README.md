## Placemaking do Museu de Arte de São Paulo

Proposta de *dashboard* para o Laboratório Arq.Futuro das Cidades do Insper.

**Autores:** Pedro Florencio & Fernando Almeida | @Iplanfor

**Acesso:** https://cautech.shinyapps.io/insper-dashboard/ (Versão 0.0.1)